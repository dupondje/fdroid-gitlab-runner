
This gitlab-runner provides two key services:

* building and deploying nightly builds of the website at
  https://staging.f-droid.org
* providing a gitlab-runner that is tailored to running Android
  emulators with KVM access
